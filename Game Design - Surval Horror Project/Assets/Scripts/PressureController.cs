﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using UnityEngine;
using UnityEngine.UI;

public class PressureController : MonoBehaviour
{
    public int plaque;
    void OnCollisionEnter (Collision col)
    {
        if(col.gameObject.name == "Player")
        {
            if (plaque == 1)
            {
                GlobalControler.pressed_one = true;
            }
            if (plaque == 2)
            {
                GlobalControler.pressed_two = true;
            }
            if (plaque == 3)
            {
                GlobalControler.pressed_three = true;
            }
            if (plaque == 4)
            {
                GlobalControler.pressed_four = true;
            }
            if (plaque == 6)
            {
                GlobalControler.pressed_six = true;
            }
            if (plaque == 7)
            {
                GlobalControler.pressed_seven = true;
            }
        }
    }
}
