﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneController : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GlobalControler.gameover = false;
            SceneManager.LoadScene("Street_Lightning");
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Application.Quit();
        }
    }
    public void LoadSceneByName(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }

    public void ByeBye()
    {
        Application.Quit();
    }
}
