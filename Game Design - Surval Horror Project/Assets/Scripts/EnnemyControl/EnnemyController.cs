﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnnemyController : MonoBehaviour
{
    public GameObject   ennemy;
    private Renderer MyRenderer;
    public GameObject   player;
    static int counter = 0;
    double  destination;
    Boolean statut = false;
    Boolean gameOver = false;
    double FocusLevel;
    public int idEnnemy;

    void Start()
    {
        MyRenderer = GetComponent<Renderer>();
    }
    void Update()
    {
        ClearConsole();
        if (!MyRenderer.isVisible)
        {
            if (idEnnemy == 1 && GlobalControler.pressed_one == true)
            {
                EnnemyBackward();
            }
            if (idEnnemy == 2 && GlobalControler.pressed_two == true)
            {
                EnnemyBackward();
            }
            if (idEnnemy == 3 && GlobalControler.pressed_three == true)
            {
                EnnemyBackward();
            }
            if (idEnnemy == 4 && GlobalControler.pressed_four == true)
            {
                EnnemyBackward();
            }
            if (idEnnemy == 6 && GlobalControler.pressed_six == true)
            {
                EnnemyBackward();
            }
            if (idEnnemy == 7 && GlobalControler.pressed_seven == true)
            {
                EnnemyBackward();
            }
            // DisplayPlayerPosition();
            // DisplayEnnemyPosition();
        }
        else
            LooseFocus();
        IsOverOrNot();
    }

    void LateUpdate()
    {
        LooseFocus();
        Debug.Log(FocusLevel);
    }

    public void LooseFocus()
    {
        if (FocusLevel > 0)
            FocusLevel -= 0.5;
    }

    public void GainFocus()
    {
        if (FocusLevel < 100)
            FocusLevel += 2;
    }

	public static void ClearConsole()
	{
		Type.GetType("UnityEditor.LogEntries,UnityEditor.dll").GetMethod("Clear", BindingFlags.Static | BindingFlags.Public).Invoke(null,null);
	}

    public void DisplayPlayerPosition()
    {
        Debug.Log("Player X= " + player.transform.position.x);
        Debug.Log("Player Z= " + player.transform.position.z);
    }

    public void DisplayEnnemyPosition()
    {
        Debug.Log("Ennemy X= " + ennemy.transform.position.x);
        Debug.Log("Ennemy Z= " + ennemy.transform.position.z);
    }

    public void EnnemyBackward()
    {
        Debug.Log("ID = " + idEnnemy + " && Counter = " + counter);

        if (counter == 0)
        {
            destination = ennemy.transform.position.z - 2.00f;
        }
        if (statut == false)
        {
            if (ennemy.transform.position.z > destination)
            {
                ennemy.transform.position = new Vector3(ennemy.transform.position.x, ennemy.transform.position.y, ennemy.transform.position.z - 0.01f);
            }
            if (ennemy.transform.position.z <= destination)
            {
                statut = true;
            }
        }
        else if (statut == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, 0.035f);            
        }
    counter++;
    }

    void IsOverOrNot()
    {
        if (GlobalControler.gameover == true)
        {
            GameObject player = GameObject.Find ("Player");

            GameObject one = GameObject.Find ("Ennemy 1");
            GameObject two = GameObject.Find ("Ennemy 2");
            GameObject three = GameObject.Find ("Ennemy 3");
            GameObject four = GameObject.Find ("Ennemy 4");
            GameObject six = GameObject.Find ("Ennemy 6");
            GameObject seven = GameObject.Find ("Ennemy 7");

            // Destroy (player.gameObject);
            // Destroy (one.gameObject);
            // Destroy (two.gameObject);
            // Destroy (three.gameObject);
            // Destroy (four.gameObject);
            // Destroy (six.gameObject);
            // Destroy (seven.gameObject);

            SceneManager.LoadScene("Loose Retry Quit");
        }
    }
}