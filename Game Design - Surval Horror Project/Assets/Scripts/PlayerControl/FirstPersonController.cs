﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{

    public float    m_speed = 10.0f;
    private float   m_translation;
    private float   m_strafe;



    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void FixedUpdate()
    {
        m_translation = Input.GetAxis("Vertical") * m_speed * Time.fixedDeltaTime;
        m_strafe = Input.GetAxis("Horizontal") * m_speed * Time.fixedDeltaTime;
        transform.Translate(m_strafe, 0, m_translation);
    }

}
