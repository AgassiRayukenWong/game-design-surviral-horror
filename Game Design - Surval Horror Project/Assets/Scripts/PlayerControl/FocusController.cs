﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusController : MonoBehaviour
{
    public float distanceToSee;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        RaycastHit hitInfo;

        if (Physics.Raycast(this.transform.position, this.transform.forward, out hitInfo, distanceToSee))
        {
            if (hitInfo.collider.gameObject.tag == "Enemy")
            {
                EnnemyController ec = hitInfo.collider.gameObject.GetComponent<EnnemyController>();
                ec.GainFocus();
            }
        }
    }
}
