﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{

    //Sensitivity multiplier
    public float m_sensitivity = 5.0f;

    //Limit of looking upward
    public float m_yRotationUpperLimit = 90.0f;
    //Limit of looking downward
    public float m_yRotationLowerLimit = 70.0f;

    //Reference to the main object holding others
    private GameObject  m_character;

    //Local rotation in degrees of the character
    private Vector2     m_mouseRotation;

    
    void Start()
    {
        m_character = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        var mouseDelta = new Vector2( Input.GetAxisRaw("Mouse X") * m_sensitivity, Input.GetAxisRaw("Mouse Y") * m_sensitivity );
        m_mouseRotation += mouseDelta;
        m_mouseRotation.y = Mathf.Clamp(m_mouseRotation.y, -m_yRotationLowerLimit, m_yRotationUpperLimit);

        transform.localRotation = Quaternion.AngleAxis(-m_mouseRotation.y, Vector3.right);
        m_character.transform.localRotation = Quaternion.AngleAxis(m_mouseRotation.x, Vector3.up);

    }
}
