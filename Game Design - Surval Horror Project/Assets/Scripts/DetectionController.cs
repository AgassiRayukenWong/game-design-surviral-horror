﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetectionController : MonoBehaviour
{
    private Renderer MyRenderer;
    public Text VisibleText;
    
    void Start()
    {
        MyRenderer = GetComponent<Renderer>();
    }
    void Update()
    {
        if (MyRenderer.isVisible)
            VisibleText.text = "Visible";
        else
            VisibleText.text = "Invisible";
    }
}
