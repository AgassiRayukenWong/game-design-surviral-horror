﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using UnityEngine;
using UnityEngine.UI;
public class GameOverController : MonoBehaviour
{
    void OnCollisionEnter (Collision col)
    {
        if (col.gameObject.name == "Ennemy 1")
        {
            GlobalControler.gameover = true;
        }
        if (col.gameObject.name == "Ennemy 2")
        {
            GlobalControler.gameover = true;
        }
        if (col.gameObject.name == "Ennemy 3")
        {
            GlobalControler.gameover = true;
        }
        if (col.gameObject.name == "Ennemy 4")
        {
            GlobalControler.gameover = true;
        }
        if (col.gameObject.name == "Ennemy 6")
        {
            GlobalControler.gameover = true;
        }
        if (col.gameObject.name == "Ennemy 7")
        {
            GlobalControler.gameover = true;
        }
    }
}
