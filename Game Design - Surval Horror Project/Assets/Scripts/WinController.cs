﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinController : MonoBehaviour
{
    void OnCollisionEnter (Collision col)
    {
        if (col.gameObject.name == "Player")
        {
            GameObject player = GameObject.Find ("Player");

            GameObject one = GameObject.Find ("Ennemy 1");
            GameObject two = GameObject.Find ("Ennemy 2");
            GameObject three = GameObject.Find ("Ennemy 3");
            GameObject four = GameObject.Find ("Ennemy 4");
            GameObject six = GameObject.Find ("Ennemy 6");
            GameObject seven = GameObject.Find ("Ennemy 7");

            // Destroy (player.gameObject);
            // Destroy (one.gameObject);
            // Destroy (two.gameObject);
            // Destroy (three.gameObject);
            // Destroy (four.gameObject);
            // Destroy (six.gameObject);
            // Destroy (seven.gameObject);

            SceneManager.LoadScene("Finish Retry Quit");
        }
    }
}
